# Repomanagement-Suite

For the Source-Code see

* [TcWebcrawler Source](https://gitlab.com/BollosEulalia/tcwebcrawler)
* [repomanager Source](https://gitlab.com/BollosEulalia/repomanager)
* [Binaries](https://gitlab.com/BollosEulalia/repomanagement-suite)



## TcWebcrawler Usage

If the page requires a login create a file called `credentials.json`.
You can use `test/testdata/credentials.json` as template.

Start with the provided configuration file at `test/testdata/webcrawler.toml` and adjust the values.

Adjust the values according to your html table. If your table doesn't provide columns like *Role* try leaving it empty.

If the table ID is not *participants* change the value in webcrawler.toml

Run with `./tcwebcrawler`


### If the webcrawler is broken

1. Navigate to the URL containing the participants table. 
2. Check that all participants are visible.
3. Press `CTRL+S`. 
4. Select `Web Page, HTML only` and save the file.
	1. See image below.
5. Run `./tcwebcrawler --in participants.html --out repolist.csv`

![Download Participants HTML Table](images/download.png)



## Repomanager Usage

### Prerequisites

#### 1. You need a CSV file that contains at least the following information:

**If your university is using teachcenter you can create the initial list with the [tcwebcrawler](https://gitlab.com/BollosEulalia/tcwebcrawler)**

This repository will be referred to as *repolist repository* or *database repository*.

	* **Name** for each participant
	* **MatriculationNr**
		- If you don't have this you can also use any value you like as long as they are unique for everyone
	
	Optional values are:
	* Groups
	* Email
	
**Example:**
	
| Name         | MatricultionNr |
|:--------------:|:-----------:|
| Student1 |739 |
| Student2     |6 |
| Student3 | hello |
	
#### 2. Create a Repository on your gitlab server

* For a template you can use the [Template Repository](https://gitlab.com/BollosEulalia/repomanager-template)
	- The configuration files are explained there
* Create an access token for this repository
	- scopes: api
	- You can also use a private token but this is not recommended.
	- Add this token to *configrepo.toml*	


#### 3. Create a group for the repositories of the participants

* Create an access token for this group
	- scopes: api
	- You can also use a private token but this is not recommended.
	- Add this token to *fullconfig.toml*
	

**For all following options the repolist.csv in your config/database repository will be updated automatically**


### Create Repositories

First run  `./repomanager --createrepos -a <assignmentname>  --dryrun` .

If the output looks good then run without the `--dryrun` flag.

`./repomanager --createrepos -a <assignmentname>`

You can create repositories for multiple assignments at once

`./repomanager ---createrepos -a <assignmentname1> <assignmentname2>`

**If you create too many repositories and get timeouts (and the default is too short)**

`./repomanager --createrpos -a <assignmentname> -w <seconds>`


### Update Database

Run `./repomanager --update -a <assignmentname>`

### Download

NOTE: Deadline provided in examples is in UTC+0

**If you don't specify a submission branch run:**

It will default to the branch with the latest commit until deadline.

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z`

**With a specific submission branch**

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z -b submission `

This will also download all other branches. If you want **only** the **specified branch** then use 

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z -b submission --ignore`

**Specify the directory/tree of the repository**

`./repomanager --download --tree directory1 directory2`

**To speed up the downloads e.g. if you only need to download a specific group or repositories containing a specific substring**

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z -g <group>`

This will only download files for students where the *Groups* column in the csv file contains this substring.

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z --search team`

You can specifiy multiple searches and groups:

`./repomanager --download --deadline 2022-04-24T23:59:59.0000Z --search team1 team2 -g group1 group1`

This will only download files for students where the *SShRepo+Assignment* (e.g. *SshRepo0*) column in the csv file contains this substring.


**Set the download directory**

It defaults to *output*.

Use the `-o` Flag.

`./repomanager --download -o /tmp/whatever/`

### Import

Run `./repomanager --update --import filename.csv`


**For other flags use the help**

`./repomanager -h`

`./repomanager --help`
